# OG.Tests

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `og_tests` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:og_tests, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/og_tests](https://hexdocs.pm/og_tests).

