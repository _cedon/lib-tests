defmodule OG.AuthTest do
  use ExUnit.Case
  doctest OG.Auth

  alias OG.Auth.User
  alias OG.Auth.Company

  describe "run/1" do
    test "when token is invalid should return error" do
      assert {:error, _} = OG.Auth.run("INVALID")
    end

    test "when token is valid should return the user" do
      assert {:ok, %User{authorizations: [%{company: %Company{}} | _]}} =
        OG.Auth.run(
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2Rldi00djZodzlmMC51cy5hdXRoMC5jb20vIiwic3ViIjoiYUM5WGZZM0VUYUdSV1I5cWd5WXI2eUJZRmdVWG55eHJAY2xpZW50cyIsImVtYWlsIjoidGhpYWdvLmxpbWFAc2VtYW50aXguY29tLmJyIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMDEwIiwiaWF0IjoxNTk0NDg1MzU0LCJleHAiOjE5OTQ1NzE3NTQsImF6cCI6ImFDOVhmWTNFVGFHUldSOXFneVlyNnlCWUZnVVhueXhyIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.VALID"
        )
    end
  end

  describe "get_authorization_by_company_id/2" do
    setup do
      {:ok, %{
        user: %User{
          id: UUID.uuid4(),
          name: "Semantix User",
          email: "opengalaxy.team@semantix.com.br",
          authorizations: [
            %{
              permissions: ["read_all", "write_all"],
              company: %Company{
                id: UUID.uuid4(),
                name: "MY ADMIN COMPANY SA"
              }
            },
            %{
              permissions: ["read_all"],
              company: %Company{
                id: UUID.uuid4(),
                name: "MY READONLY COMPANY SA"
              }
            }
          ]
        }
      }}
    end

    test "when user is not authorized should return error", %{user: user} do
      assert {:error, :authorization_not_found} =
        OG.Auth.get_authorization_by_company_id(user, UUID.uuid4())
    end

    test "when user is authorized should return authorization", %{user: user = %{authorizations: [%{company: %{id: company_id}} | _]}} do
      assert {:ok, %{company: %{id: ^company_id}, permissions: _}} =
        OG.Auth.get_authorization_by_company_id(user, company_id)
    end
  end

  describe "has_permission?/3" do
    setup do
      {:ok, %{
        user: %User{
          id: UUID.uuid4(),
          name: "Semantix User",
          email: "opengalaxy.team@semantix.com.br",
          authorizations: [
            %{
              permissions: ["read_all", "write_all"],
              company: %Company{
                id: UUID.uuid4(),
                name: "MY ADMIN COMPANY SA"
              }
            },
            %{
              permissions: ["read_all"],
              company: %Company{
                id: UUID.uuid4(),
                name: "MY READONLY COMPANY SA"
              }
            }
          ]
        }
      }}
    end

    test "when the user has permission in the company should return true", %{user: user = %{authorizations: [%{company: %{id: company_id}, permissions: [perm | _]} | _]}} do
      assert OG.Auth.has_permission?(user, company_id, perm)
    end

    test "when the user does not has permission in the company should return false", %{user: user = %{authorizations: [%{company: %{id: company_id}} | _]}} do
      assert {false, [company: %OG.Auth.Company{id: company_id, name: "MY ADMIN COMPANY SA"}]} =
       OG.Auth.has_permission?(user, company_id, "permission")
    end

    test "when user is not authorized in company should return error", %{user: user} do
      assert {:error, :authorization_not_found} =
        OG.Auth.has_permission?(user, UUID.uuid4(), "permission")
    end
  end

  describe "get_permission_by_method/1" do
    test "when the method exists is GET should return `read_all` permission" do
      assert {:ok, "read_all"} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "GET"})
    end

    test "when the method exists is POST should return `write_all` permission" do
      assert {:ok, "write_all"} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "POST"})
    end

    test "when the method exists is PUT should return `write_all` permission" do
      assert {:ok, "write_all"} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "PUT"})
    end

    test "when the method exists is PATCH should return `write_all` permission" do
      assert {:ok, "write_all"} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "PATCH"})
    end

    test "when the method exists is DELETE should return `delete_all` permission" do
      assert {:ok, "delete_all"} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "DELETE"})
    end

    test "when the method is invalid should return error"  do
      assert {:error, :method_not_found} =
        OG.Auth.get_permission_by_method(%Plug.Conn{method: "INVALID"})
    end
  end

  describe "get_company_id_from_url/1" do
    test "when company id is present and is valid should return ok" do
      assert {:ok, "5945137c-400f-48ea-af17-131ab18d1e82"} =
        OG.Auth.get_company_id_from_url(%Plug.Conn{path_info: ["v1", "companies", "5945137c-400f-48ea-af17-131ab18d1e82"]})
    end

    test "when company id is present and is not valid should return error" do
      assert {:error, :invalid_id} =
        OG.Auth.get_company_id_from_url(%Plug.Conn{path_info: ["v1", "companies", "not-a-valid-id"]})
    end

    test "when company id is not present should return ok"  do
      assert {:ok, "force_permission"} =
        OG.Auth.get_company_id_from_url(%Plug.Conn{path_info: ["v1", "companies"]})
    end
  end

end
