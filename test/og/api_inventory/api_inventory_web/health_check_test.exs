defmodule OG.ApiInventoryWeb.HealthCheckTest do
  use ApiInventoryWeb.ConnCase

  describe "call/2" do
    test "should return 200", %{conn: conn} do
      assert %Plug.Conn{status: 200} =
        ApiInventoryWeb.HealthCheck.call(conn, [])
    end

    test "should format response as json", %{conn: conn} do
      conn = ApiInventoryWeb.HealthCheck.call(conn, [])
      json = json_response(conn, 200)

      assert json == %{
        "status" => "ok",
        "checks" => []
      }
    end
  end
end
