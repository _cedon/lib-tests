defmodule OG.ApiInventoryWeb.ResourceTemplateControllerTest do
  use ApiInventoryWeb.ConnCase

  import ApiInventory.Factory
  import OpenApiSpex.TestAssertions

  describe "GET /v1/resource_templates" do
    setup do
      {:ok, %{
        template: insert(:active_resource_template),
        company_id: "c948b2de-7957-432c-b542-d31ddce4df14",
        token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMDljMmIzYS0xYWY0LTQxODUtOGI5Yi1lYjJkZTE3MjJjNWUiLCJuYW1lIjoiT3BlbiBHYWxheHkgVGVhbSIsImlhdCI6MTUxNjIzOTAyMiwiZW1haWwiOiJvcGVuZ2FsYXh5LnRlYW1Ac2VtYW50aXguY29tLm5yIn0.VALID"
      }}
    end

    test "should return all resource templates", %{conn: conn, template: %{id: template_id}, token: token} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_template_path(conn, :index)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> get(path)

      json = json_response(conn, 200)

      assert_schema(json, "ListResourceTemplateResponse", api_spec)

      assert %{"entries" => [%{"id" => ^template_id}]} = json
    end
  end
end
