defmodule OG.ApiInventoryWeb.ResourceControllerTest do
  use ApiInventoryWeb.ConnCase

  import ApiInventory.Factory
  import OpenApiSpex.TestAssertions

  setup do
    {:ok, %{
      company_id: "c948b2de-7957-432c-b542-d31ddce4df14",
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMDljMmIzYS0xYWY0LTQxODUtOGI5Yi1lYjJkZTE3MjJjNWUiLCJuYW1lIjoiT3BlbiBHYWxheHkgVGVhbSIsImlhdCI6MTUxNjIzOTAyMiwiZW1haWwiOiJvcGVuZ2FsYXh5LnRlYW1Ac2VtYW50aXguY29tLm5yIn0.VALID"
    }}
  end

  describe "POST /v1/companies/:company_id/resources" do
    setup do
      {:ok,
        params: %{
          template_id: insert(:active_resource_template).id
        }
      }
    end

    @tag :controllers_post_resources
    test "when receive valid data should create resource and return 201", %{conn: conn, token: token, params: params, company_id: company_id} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :create, company_id)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> post(path, params)

      json = json_response(conn, 201)

      assert_schema(json, "ResourceResponse", api_spec)
    end

    @tag :controllers_post_resources
    test "when attempt to create two resources of same type should return 409", %{conn: conn, token: token, company_id: company_id} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      group = insert(:resource_group, company_id: company_id)

      %{template_id: template_id} =
        insert(:resource,
          status: "PROVISIONED",
          group: group
        )

      params = %{"template_id" => template_id}
      path   = Routes.resource_path(conn, :create, company_id)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> post(path, params)

      json = json_response(conn, 409)

      assert_schema(json, "ErrorResponse", api_spec)
    end

    @tag :controllers_post_resources
    test "when receive invalid template_id should return 400", %{conn: conn, token: token, params: params, company_id: company_id} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()
      params   = Map.put(params, "template_id", Ecto.UUID.generate())

      path = Routes.resource_path(conn, :create, company_id)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> post(path, params)

      json = json_response(conn, 404)

      assert_schema(json, "ErrorResponse", api_spec)
    end

    @tag :controllers_post_resources
    test "when token is not sent should return 401", %{conn: conn} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :index, Ecto.UUID.generate())
      conn = post(conn, path)
      json = json_response(conn, 401)

      assert_schema(json, "ErrorResponse", api_spec)
    end
  end

  describe "DELETE /v1/companies/:company_id/resources" do
    setup %{company_id: company_id} do
      {:ok,
        resource_group: insert(:resource_group, company_id: company_id)
      }
    end

    setup %{resource_group: resource_group} do
      {:ok, resource: insert(:resource,
        group: resource_group,
        status: "PROVISIONED"
      )}
    end

    @tag :controllers_post_resources
    test "when receive valid data should delete resource and return 202", %{conn: conn, token: token, resource: %{id: resource_id}, company_id: company_id} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :delete, company_id, resource_id)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> delete(path)

      json = json_response(conn, 202)

      assert_schema(json, "ResourceResponse", api_spec)
    end
  end

  describe "GET /v1/companies/:company_id/resources" do
    test "when user attempt to access a company without authorization should return 403", %{conn: conn, token: token} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :index, Ecto.UUID.generate())

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> post(path)

      json = json_response(conn, 403)

      assert_schema(json, "ErrorResponse", api_spec)
    end

    test "when user have permission should return 200", %{conn: conn, token: token, company_id: company_id} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :index, company_id)

      conn =
        conn
        |> put_req_header("authorization", "bearer #{token}")
        |> get(path)

      json = json_response(conn, 200)

      assert_schema(json, "ListResourceResponse", api_spec)
    end

    test "when token is not sent should return 401", %{conn: conn} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :index, Ecto.UUID.generate())
      conn = get(conn, path)
      json = json_response(conn, 401)

      assert_schema(json, "ErrorResponse", api_spec)
    end

    test "when token is invalid should return 401", %{conn: conn} do
      api_spec = ApiInventoryWeb.ApiSpec.spec()

      path = Routes.resource_path(conn, :index, Ecto.UUID.generate())

      conn =
        conn
        |> put_req_header("authorization", "bearer #{Ecto.UUID.generate()}")
        |> get(path)

      json = json_response(conn, 401)

      assert_schema(json, "ErrorResponse", api_spec)
    end
  end
end
