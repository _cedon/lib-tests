ExUnit.start()
Faker.start()

Ecto.Adapters.SQL.Sandbox.mode(ApiInventory.Repo, :manual)

Application.ensure_all_started(:ex_machina)