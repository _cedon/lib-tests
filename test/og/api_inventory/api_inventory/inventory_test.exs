defmodule ApiInventory.InventoryTest do
  use ApiInventory.DataCase

  alias ApiInventory.Inventory
  alias ApiInventory.Inventory.Resource
  alias ApiInventory.Inventory.ResourceGroup

  import ApiInventory.Factory

  describe "create_resource_group/1" do
    setup do
      {:ok, %{params: %{
        "company_id" => Ecto.UUID.generate(),
        "company_name" => Faker.Company.name()
      }}}
    end

    @tag :inventory_create_resource_group
    test "when receive nil company_id should returns error", %{params: params} do
      params = Map.put(params, "company_id", nil)

      assert {:error, %Ecto.Changeset{errors: [company_id: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive empty company_id should returns error", %{params: params} do
      params = Map.put(params, "company_id", "")

      assert {:error, %Ecto.Changeset{errors: [company_id: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive no company_id should returns error", %{params: params} do
      params = Map.drop(params, ["company_id"])

      assert {:error, %Ecto.Changeset{errors: [company_id: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive nil company_name should returns error", %{params: params} do
      params = Map.put(params, "company_name", nil)

      assert {:error, %Ecto.Changeset{errors: [company_name: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive empty company_name should returns error", %{params: params} do
      params = Map.put(params, "company_name", "")

      assert {:error, %Ecto.Changeset{errors: [company_name: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive no company_name should returns error", %{params: params} do
      params = Map.drop(params, ["company_name"])

      assert {:error, %Ecto.Changeset{errors: [company_name: {"can't be blank", _}]}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive valid data should creates a template", %{params: params} do
      assert {:ok, %ResourceGroup{}} =
        Inventory.create_resource_group(params)
    end

    @tag :inventory_create_resource_group
    test "when receive valid data should generate and set a slug", %{params: params} do
      assert {:ok, %ResourceGroup{slug: slug}} =
        Inventory.create_resource_group(params)

      refute is_nil(slug)
    end
  end

  describe "create_resource/1" do
    setup do
      {:ok, %{
        resource_template: insert(:active_resource_template)
      }}
    end

    setup %{resource_template: resource_template} do
      {:ok, %{
        params: %{
          "template_id" => resource_template.id
        },
        company: %{
          id: Ecto.UUID.generate(),
          name: Faker.Company.name()
        },
        resource_template: resource_template
      }}
    end

    @tag :inventory_create_resource
    test "when receive valid data should generate and set a slug", %{params: params, company: company} do
      assert {:ok, %Resource{slug: slug, resource_number: resource_number}} =
        Inventory.create_resource(params, company)

      assert String.last(slug) == Integer.to_string(resource_number)
    end

    @tag :inventory_create_resource
    test "when receive valid data should set status as 'PROVISIONING'", %{params: params, company: company} do
      assert {:ok, %Resource{status: "PROVISIONING"}} =
        Inventory.create_resource(params, company)
    end

    @tag :inventory_create_resource
    test "when receive valid data should set type as de same of ResourceTemplate", %{resource_template: %{type: type}, params: params, company: company} do
      assert {:ok, %Resource{type: ^type}} =
        Inventory.create_resource(params, company)    end

    @tag :inventory_create_resource
    test "when receive empty template_id should returns error", %{params: params, company: company} do
      Enum.each([nil, ""], fn template_id ->
        assert {:error, %Ecto.Changeset{errors: errors}} =
          params
          |> Map.merge(%{"template_id" => template_id})
          |> Inventory.create_resource(company)

        Enum.each(errors, fn {_, error} ->
          assert {"can't be blank", _} = error
        end)

        assert Enum.map(errors, fn {field, _} -> field end) == [:type, :slug, :template_id]
      end)
    end

    @tag :inventory_create_resource
    test "when receive no template_id should returns error", %{params: params, company: company} do
      assert {:error, %Ecto.Changeset{errors: errors}} =
        params
        |> Map.drop(["template_id"])
        |> Inventory.create_resource(company)

      Enum.each(errors, fn {_, error} ->
        assert {"can't be blank", _} = error
      end)

      assert Enum.map(errors, fn {field, _} -> field end) == [:type, :slug, :template_id]
    end

    @tag :inventory_create_resource
    test "when receive inexistent template_id should returns error", %{params: params, company: company} do
      assert {:error, :template_not_found} =
        params
        |> Map.put("template_id", Ecto.UUID.generate())
        |> Inventory.create_resource(company)
    end

    @tag :skip
    @tag :inventory_create_resource
    test "when creates two resources of same type in namespace should increment resource_number", %{params: params, company: company} do
      Enum.each(1..10, fn n ->
        assert {:ok, %Resource{resource_number: ^n}} =
          Inventory.create_resource(params, company)
      end)
    end

    @tag :inventory_create_resource
    test "when message publication failed should return error", %{params: params, company: company} do
      topic = Application.get_env(:api_inventory, :gcp_pubsub_topic)
      Application.put_env(:api_inventory, :gcp_pubsub_topic, "failed")

      assert {:error, {:event, _}} = Inventory.create_resource(params, company)

      Application.put_env(:api_inventory, :gcp_pubsub_topic, topic)
    end
  end

  describe "create_resource/1 when already have deleted resources of same type" do
    setup do
      {:ok, %{
        resource_template: insert(:active_resource_template)
      }}
    end

    setup %{resource_template: resource_template} do
      {:ok, %{
        params: %{
          "template_id" => resource_template.id
        },
        company: %{
          id: Ecto.UUID.generate(),
          name: Faker.Company.name()
        }
      }}
    end

    setup %{company: %{id: company_id}} do
      {:ok,
        resource_group: insert(:resource_group, company_id: company_id)
      }
    end

    setup %{resource_group: resource_group, resource_template: resource_template} do
      {:ok, resource: insert(:resource,
        status: "DELETED",
        group: resource_group,
        template: resource_template
      )}
    end

    test "should return error", %{params: params, company: company} do
      assert {:ok, %Resource{status: "PROVISIONING"}} =
        Inventory.create_resource(params, company)
    end
  end

  describe "create_resource/1 already have same type resources" do
    setup do
      {:ok, %{
        resource_template: insert(:active_resource_template)
      }}
    end

    setup %{resource_template: resource_template} do
      {:ok, %{
        params: %{
          "template_id" => resource_template.id
        },
        company: %{
          id: Ecto.UUID.generate(),
          name: Faker.Company.name()
        }
      }}
    end

    setup %{company: %{id: company_id}} do
      {:ok,
        resource_group: insert(:resource_group, company_id: company_id)
      }
    end

    setup %{resource_group: resource_group, resource_template: resource_template} do
      {:ok, resource: insert(:resource,
        status: "PROVISIONED",
        group: resource_group,
        template: resource_template
      )}
    end

    test "should return error", %{params: params, company: company} do
      assert {:error, :resource_type_conflict} =
        Inventory.create_resource(params, company)
    end
  end

  describe "get_resources/1" do
    test "when have no resources should return a empty array" do
      resources = Inventory.get_resources(Ecto.UUID.generate())

      assert is_list(resources)
      assert Enum.empty?(resources)
    end
  end

  describe "get_resources/1 when have resources" do
    setup do
      {:ok, resource_group: insert(:resource_group)}
    end

    setup %{resource_group: resource_group} do
      {:ok,
        resource: insert(:resource, group: resource_group),
        deleted_resource: insert(:resource,
          status: "DELETED",
          group: resource_group
        )
      }
    end

    test "should return all resources related with company", %{resource: %{id: resource_id, group: %{company_id: company_id}}} do
      %Resource{id: another_resource_id, group: %{company_id: another_company_id}} = insert(:resource)

      assert [%Resource{id: ^resource_id}] = Inventory.get_resources(company_id)
      assert [%Resource{id: ^another_resource_id}] = Inventory.get_resources(another_company_id)
    end

    test "shouldn't return DELETED resources", %{deleted_resource: %{id: deleted_resource_id, group: %{company_id: company_id}}} do
      company_id
      |> Inventory.get_resources()
      |> Enum.each(fn %{id: resource_id} ->
        refute resource_id == deleted_resource_id
      end)
    end
  end

  describe "update_resource/1" do
    setup do
      {:ok, %{
        resource: insert(:resource, status: "PROVISIONING")
      }}
    end

    @tag :inventory_update_resource
    test "when have endpoints should validate url", %{resource: %Resource{id: resource_id}} do
      valid_urls = [
        "#{Ecto.UUID.generate()}.apps.v2.opengalaxy.io",
        "#{Ecto.UUID.generate()}.apps.v2.opengalaxy.io/path",

        "https://#{Ecto.UUID.generate()}.apps.v2.opengalaxy.io",
        "https://#{Ecto.UUID.generate()}.apps.v2.opengalaxy.io/path",
      ]

      endpoints =
        Enum.map(valid_urls, fn url ->
          Map.new()
          |> Map.put("url", url)
          |> Map.put("name", Faker.Lorem.word())
        end)

      assert {:ok, %Resource{endpoints: saved_endpoints}} =
        Map.new()
        |> Map.put("id", resource_id)
        |> Map.put("endpoints", endpoints)
        |> Inventory.update_resource()

      Enum.zip([endpoints, saved_endpoints])
      |> Enum.map(fn {%{"url" => url}, %{url: saved_url}} ->
        assert url == saved_url
      end)
    end

    @tag :inventory_update_resource
    test "when have no resources should return error" do
      {:error, :resource_not_found} =
        Map.new()
        |> Map.put("id", Ecto.UUID.generate())
        |> Inventory.update_resource()
    end

    @tag :inventory_update_resource
    test "when resource exists should update it", %{resource: %Resource{id: resource_id, status: status}} do
      new_status = "PROVISIONED"

      refute status == new_status

      assert {:ok, %Resource{status: ^new_status}} =
        Map.new()
        |> Map.put("id", resource_id)
        |> Map.put("status", new_status)
        |> Inventory.update_resource()
    end
  end

  describe "delete_resource/1" do
    test "when resource is PROVISIONED should mark as DELETING" do
      resource = insert(:resource, status: "PROVISIONED")

      assert {:ok, %Resource{status: "DELETING"}} =
        Inventory.delete_resource(resource)
    end

    test "when resource is DELETING should return ok" do
      resource = insert(:resource, status: "DELETING")

      assert {:ok, %Resource{status: "DELETING"}} =
        Inventory.delete_resource(resource)
    end

    test "when resource is DELETED should return ok" do
      resource = insert(:resource, status: "DELETED")

      assert {:ok, %Resource{status: "DELETED"}} =
        Inventory.delete_resource(resource)
    end

    test "when resource isn't PROVISIONED should return error" do
      resource = insert(:resource, status: "PROVISIONING")

      assert {:error, :resource_not_provisioned} =
        Inventory.delete_resource(resource)
    end
  end

  describe "get_resource/2" do
    setup do
      {:ok, company_id: Ecto.UUID.generate()}
    end

    setup %{company_id: company_id} do
      {:ok,
        resource_group: insert(:resource_group, company_id: company_id)
      }
    end

    setup %{resource_group: resource_group} do
      {:ok, resource: insert(:resource, group: resource_group)}
    end

    test "when resource not found should return error", %{company_id: company_id} do
      assert {:error, :resource_not_found} =
        Inventory.get_resource(Ecto.UUID.generate(), company_id)
    end

    test "when resource is not owned by the company should return error", %{resource: %{id: resource_id}} do
      assert {:error, :resource_not_found} =
        Inventory.get_resource(resource_id, Ecto.UUID.generate())
    end

    test "when resource is owned by the company should return it", %{resource: %{id: resource_id}, company_id: company_id} do
      assert {:ok, %Resource{id: ^resource_id}} =
        Inventory.get_resource(resource_id, company_id)
    end
  end
end
