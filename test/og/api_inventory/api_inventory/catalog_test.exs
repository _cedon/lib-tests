defmodule OG.ApiInventory.CatalogTest do
  use ApiInventory.DataCase

  import ApiInventory.Factory

  alias ApiInventory.Catalog
  alias ApiInventory.Catalog.ResourceTemplate

  describe "get_resource_templates/0" do
    setup do
      {:ok, %{
        active_templates: insert_list(4, :active_resource_template),
        inactive_templates: insert_list(4, :inactive_resource_template)
      }}
    end

    @tag :catalog_get_resource_templates
    test "should return a list of %ResourceTemplate{}" do
      assert [%ResourceTemplate{} | _] = Catalog.get_resource_templates()
    end

    @tag :catalog_get_resource_templates
    test "should return only ACTIVE %ResourceTemplate{}" do
      assert length(Catalog.get_resource_templates()) == 4

      Enum.each(Catalog.get_resource_templates(), fn %ResourceTemplate{status: status} ->
        assert "ACTIVE" = status
      end)
    end
  end

  describe "create_resource_template/1" do
    setup do
      {:ok, %{
        params: %{
          "name" => "NIFI",
          "type" => "NIFI",

          "settings" => %{}
        }
      }}
    end

    @tag :catalog_create_resource_template
    test "when receive valid data should set status as ACTIVE", %{params: params} do
      params = Map.put(params, "status", "INACTIVE")

      assert {:ok, %ResourceTemplate{status: "ACTIVE"}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive valid data should creates a template", %{params: params} do
      assert {:ok, %ResourceTemplate{}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive empty name should returns error", %{params: params} do
      params = Map.put(params, "name", "")

      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive nil name should returns error", %{params: params} do
      params = Map.put(params, "name", nil)

      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive no name should returns error", %{params: params} do
      params = Map.drop(params, ["name"])

      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive empty type should returns error", %{params: params} do
      params = Map.put(params, "type", "")

      assert {:error, %Ecto.Changeset{errors: [type: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive nil type should returns error", %{params: params} do
      params = Map.put(params, "type", nil)

      assert {:error, %Ecto.Changeset{errors: [type: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive no type should returns error", %{params: params} do
      params = Map.drop(params, ["type"])

      assert {:error, %Ecto.Changeset{errors: [type: {"can't be blank", _}]}} =
        Catalog.create_resource_template(params)
    end

    @tag :catalog_create_resource_template
    test "when receive invalid type should returns error", %{params: params} do
      params = Map.put(params, "type", Faker.Name.name())

      assert {:error, %Ecto.Changeset{errors: [type: {"is invalid", _}]}} =
        Catalog.create_resource_template(params)
    end
  end
end
