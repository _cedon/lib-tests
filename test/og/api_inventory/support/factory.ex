defmodule ApiInventory.Factory do
  @moduledoc false

  use ExMachina.Ecto,
    repo: ApiInventory.Repo

  alias Faker.Lorem.Shakespeare

  @doc false
  def active_resource_template_factory do
    %ApiInventory.Catalog.ResourceTemplate{
      name: "NIFI",
      type: "NIFI",

      status: "ACTIVE",
      settings: Map.new()
    }
  end

  @doc false
  def inactive_resource_template_factory do
    %ApiInventory.Catalog.ResourceTemplate{
      name: "NIFI",
      type: "NIFI",

      status: "INACTIVE",
      settings: Map.new(),
      description: Shakespeare.hamlet()
    }
  end

  @doc false
  def resource_group_factory do
    %ApiInventory.Inventory.ResourceGroup{
      slug: Faker.Internet.slug(),
      company_id: Ecto.UUID.generate()
    }
  end

  @doc false
  def resource_factory do
    %ApiInventory.Inventory.Resource{
      type: "PRESTO",
      slug: "1-presto",
      status: "ACTIVE",
      resource_number: 1,
      group: build(:resource_group),
      template: build(:active_resource_template)
    }
  end
end
