defmodule OG.Auth.PlugTest do
  use Plug.Test
  use ExUnit.Case

  alias OG.Auth.User

  setup do
    {:ok, %{conn: conn(:get, "/")}}
  end

  describe "call/2" do
    test "when authorization header have no token should return 401", %{conn: conn} do
      assert %Plug.Conn{status: 401, halted: true} =
        OG.Auth.Plug.call(conn, [])
    end

    test "when authorization header have a invalid token should return 401", %{conn: conn} do
      assert %Plug.Conn{status: 401, halted: true} =
        conn
        |> put_req_header("authorization", "bearer INVALID")
        |> OG.Auth.Plug.call([])
    end

    test "when authorization header have no bearer keyword should return 401", %{conn: conn} do
      assert %Plug.Conn{status: 401, halted: true} =
        conn
        |> put_req_header("authorization", "INVALID")
        |> OG.Auth.Plug.call([])
    end



    test "when token is valid and user does have the needed permission conn cannot be halted", %{conn: conn} do
      assert %Plug.Conn{assigns: %{user: %User{}}, status: nil, halted: false} =
        conn
        |> put_req_header("authorization", "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2Rldi00djZodzlmMC51cy5hdXRoMC5jb20vIiwic3ViIjoiYUM5WGZZM0VUYUdSV1I5cWd5WXI2eUJZRmdVWG55eHJAY2xpZW50cyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAxMCIsImVtYWlsIjoidGhpYWdvLmxpbWFAc2VtYW50aXguY29tLmJyIiwiaWF0IjoxNTk0NDg1MzU0LCJleHAiOjE1OTQ1NzE3NTQsImF6cCI6ImFDOVhmWTNFVGFHUldSOXFneVlyNnlCWUZnVVhueXhyIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.VALID")
        |> OG.Auth.Plug.call([])
    end
  end

  describe "call/2 DELETE" do

    setup do
      {:ok, %{conn: conn(:delete, "/")}}
    end

    test "when token is valid but user does not have the needed permission should return 403", %{conn: conn} do
      assert %Plug.Conn{assigns: %{user: %User{}}, status: 403, halted: true} =
        conn
        |> put_req_header("authorization", "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2Rldi00djZodzlmMC51cy5hdXRoMC5jb20vIiwic3ViIjoiYUM5WGZZM0VUYUdSV1I5cWd5WXI2eUJZRmdVWG55eHJAY2xpZW50cyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAxMCIsImVtYWlsIjoidGhpYWdvLmxpbWFAc2VtYW50aXguY29tLmJyIiwiaWF0IjoxNTk0NDg1MzU0LCJleHAiOjE1OTQ1NzE3NTQsImF6cCI6ImFDOVhmWTNFVGFHUldSOXFneVlyNnlCWUZnVVhueXhyIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.VALID")
        |> OG.Auth.Plug.call([])
    end
  end
end
