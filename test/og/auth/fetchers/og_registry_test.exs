defmodule OG.Auth.Fetchers.OGRegistryTest do
  @moduledoc false

  import Tesla.Mock
  import ExUnit.CaptureLog

  use Plug.Test
  use ExUnit.Case

  alias OG.Auth.Company
  alias OG.Auth.User

  alias OG.Auth.Fetchers.OGRegistry

  setup do
    mock(fn
      %{method: :get, headers: [{"authorization", "bearer VALID"} | _]} ->
        %Tesla.Env{
          status: 200,
          body: ~s({
            "authorizations": [
              {
                "company": {
                  "id": "c948b2de-7957-432c-b542-d31ddce4df14",
                  "name": "MY ADMIN COMPANY SA"
                },
                "permissions": ["read_all", "write_all"]
              },
              {
                "company": {
                  "id": "c4c2030e-88d3-4b63-b697-5116d609cd22",
                  "name": "MY READONLY COMPANY SA"
                },
                "permissions": ["read_all"]
              }
            ],
            "email": "opengalaxy.team@semantix.com.br",
            "id": "affe0706-cda3-11ea-87d0-0242ac130003",
            "name": "Semantix User"
          })
        }

        %{method: :get, headers: [{"authorization", "bearer INVALID"} | _]} ->
          %Tesla.Env{
            status: 401,
            body: ~s({})
          }
      end)

    :ok
  end

  describe "get_user_by_payload/2" do
    test "when registry returns 200 should return %User{}" do
      assert {:ok, %User{authorizations: [%{company: %Company{}} | _]}} =
        OGRegistry.get_user_by_payload(1, [token: "VALID"])
    end

    test "when registry returns 401 should return error" do
      assert {:error, {:status, 401}} =
        OGRegistry.get_user_by_payload(1, [token: "INVALID"])
    end

    test "when registry returns error should log this error" do
      assert capture_log(fn -> OGRegistry.get_user_by_payload(1, [token: "INVALID"]) end) =~
        "Registry returns `401` with body `{}`"
    end
  end
end
