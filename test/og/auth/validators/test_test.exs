defmodule OG.Auth.Validators.TestTests do
  use ExUnit.Case
  doctest OG.Auth

  alias OG.Auth.Validators.Test

  setup do
    {:ok, %{payload: %{
      "exp"  => 1_895_961_600,
      "iat"  => 1_516_239_022,
      "sub"  => UUID.uuid4(),
      "name" => "John Doe",
    }}}
  end

  describe "run/1" do
    setup %{payload: payload} do
      {:ok, token: %{
        header: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
        payload: (
          payload
          |> Jason.encode!()
          |> Base.encode64()
        )
      }}
    end

    test "when token finish with VALID should return user", %{token: %{header: header, payload: payload}, payload: decoded_payload} do
      assert {:ok, ^decoded_payload} = 
        [header, payload, "VALID"]
        |> Enum.join(".")
        |> Test.run()
    end

    test "when token does not finish with VALID should return error", %{token: %{header: header, payload: payload}} do
      random_signature = 
        :crypto.strong_rand_bytes(10)
        |> Base.encode64()
        |> binary_part(0, 10)

      assert {:error, :unauthorized} = 
        [header, payload, random_signature]
        |> Enum.join(".")
        |> Test.run()
    end

    test "when token is not a JWT should return error" do
      token = 
        :crypto.strong_rand_bytes(32)
        |> Base.encode64()
        |> binary_part(0, 32)

      assert {:error, :invalid_token} =
        Test.run(token)
    end
  end
end
  