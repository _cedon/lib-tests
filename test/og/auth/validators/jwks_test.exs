defmodule OG.Auth.Validators.JwksTests do
  use ExUnit.Case
  doctest OG.Auth

  import Tesla.Mock
  import ExUnit.CaptureLog

  alias OG.Auth.Validators.Jwks

  setup do
    mock(fn
      %{method: :get} -> 
        %Tesla.Env{
          status: 200,
          body: Jason.encode!(%{
            keys: [
              %{
                "alg" => "RS256",
                "kty" => "RSA",
                "use" => "sig",
                "n" => "7gB902UkV-vFwQRMcmOzj1pAtHb4xd4JSHMyxPsrBZ1kqzWOYeMcu2ReMwT4_QzkL2x5JLg4gQvFb4dX-5g9AyxmV5PZhWTMPnm1IU2IvjE4B4Wbo0WxJrOrhXlH37oKMvqdMsKPgDCoAS6YdjVzZjf91GJub0aOLhkYWBRnwJ7TNHbegVc6o5HWQlvLVYNQbEz4IsljpXftcbx5gMAI-bJ7f1rV45f3JipKegBr9fsVJXoh8kmiKjF9LaSZRgVVoZL5CxmYlhIs7l0DJ8WncPb-cUmIexnkq8JgvNBthlhbt-GASWiZ0teo5hvnTzY66HmIYWdrTrXK58_ErmIlEw",
                "e" => "AQAB",
                "kid" => "snA7etZUVx5wrZzybKklA",
                "x5t" => "OnAmBMppbY19yub272kxzpOON7Y",
                "x5c" => [
                  "MIIDDTCCAfWgAwIBAgIJZ2Gd1FnQNh7TMA0GCSqGSIb3DQEBCwUAMCQxIjAgBgNVBAMTGWRldi00djZodzlmMC51cy5hdXRoMC5jb20wHhcNMjAwNzA3MTM0MjI4WhcNMzQwMzE2MTM0MjI4WjAkMSIwIAYDVQQDExlkZXYtNHY2aHc5ZjAudXMuYXV0aDAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7gB902UkV+vFwQRMcmOzj1pAtHb4xd4JSHMyxPsrBZ1kqzWOYeMcu2ReMwT4/QzkL2x5JLg4gQvFb4dX+5g9AyxmV5PZhWTMPnm1IU2IvjE4B4Wbo0WxJrOrhXlH37oKMvqdMsKPgDCoAS6YdjVzZjf91GJub0aOLhkYWBRnwJ7TNHbegVc6o5HWQlvLVYNQbEz4IsljpXftcbx5gMAI+bJ7f1rV45f3JipKegBr9fsVJXoh8kmiKjF9LaSZRgVVoZL5CxmYlhIs7l0DJ8WncPb+cUmIexnkq8JgvNBthlhbt+GASWiZ0teo5hvnTzY66HmIYWdrTrXK58/ErmIlEwIDAQABo0IwQDAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBSFLb3f/mr9VainCLBVVM6AGYJ+GDAOBgNVHQ8BAf8EBAMCAoQwDQYJKoZIhvcNAQELBQADggEBAMd/cEWwuFGWaCwZB0CEoky53+019UPgGb1U9EpeX17Oob2YgTc3+byvR+fM7l2UIj5r5dLN+eszLQ8jpoH8yWyNako0cKQwsK2pvNmJEMnfWyMONg0VjggdJ57XKF7V0zpS1wfA7nNMJeMAMVox/y1m+cNGk3DAGHe19/qKqI0MHJPYwULeQjRdxDUXMAhJg1eJNUbMKUdV/ehrHWWKh9Lx7w7AKW/vSrdo73B8JOrHpIKgGLfyC1+lg6K7FmOitAR+ftSK7xlyCtPMnzVJEwfArlGr2PJUUyaaOmGcwEzZ1fT4G/cmBZk1zMfneQxgkCQ4/wGchOY95ShmK2jhYgI="
                ]
              },
              %{
                "alg" => "RS256",
                "kty" => "RSA",
                "use" => "sig",
                "n" => "tptMV-QFS3NB2mKm2doSYYstKawdNaZUCLLETVlPuumFZipf8m9dLMZ1nFdUOABK5gGVku8zka5KESmPiWWim7tO9odzI1UN83nY0OC1dXyvt88qgU5XBCJm29GncfuGZ9sMLbvQoi6zcec8-Kd8nopk9R2FkD29jevKHf3NLO1Ljh4_h8NTDlAoSqB3gFCc8woYA2V2cwNj4VbIPReCpjr9fiFUbfs6tK6HpkCrTJ4bcCqNSNlTExjzG99Dj77vqvWn4Y6JoLuUmIuTdROS2twoYIszXaUrXQr0rkr3ygs1zgHhcob7UUd7OT5UaQ4VgIvtT_tzT4nhyfpYvkzpKw",
                "e" => "AQAB",
                "kid" => "8L6SIFT9h0H1MLkR43HuN",
                "x5t" => "twaxYppoo8Bj9YtCiFzStFQb53Q",
                "x5c"=> [
                  "MIIDDTCCAfWgAwIBAgIJVqIB5FjxC/r/MA0GCSqGSIb3DQEBCwUAMCQxIjAgBgNVBAMTGWRldi00djZodzlmMC51cy5hdXRoMC5jb20wHhcNMjAwNzA3MTM0MjI5WhcNMzQwMzE2MTM0MjI5WjAkMSIwIAYDVQQDExlkZXYtNHY2aHc5ZjAudXMuYXV0aDAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtptMV+QFS3NB2mKm2doSYYstKawdNaZUCLLETVlPuumFZipf8m9dLMZ1nFdUOABK5gGVku8zka5KESmPiWWim7tO9odzI1UN83nY0OC1dXyvt88qgU5XBCJm29GncfuGZ9sMLbvQoi6zcec8+Kd8nopk9R2FkD29jevKHf3NLO1Ljh4/h8NTDlAoSqB3gFCc8woYA2V2cwNj4VbIPReCpjr9fiFUbfs6tK6HpkCrTJ4bcCqNSNlTExjzG99Dj77vqvWn4Y6JoLuUmIuTdROS2twoYIszXaUrXQr0rkr3ygs1zgHhcob7UUd7OT5UaQ4VgIvtT/tzT4nhyfpYvkzpKwIDAQABo0IwQDAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQA3PVd0u8SyH6jAu3kfYnpmQJs5TAOBgNVHQ8BAf8EBAMCAoQwDQYJKoZIhvcNAQELBQADggEBAGxWoKkVbwH4QAr6ByPzgWStlscFbtXn6q0mLcvpbLEWY0i8l9FTGj2g1s/bd/85vnB9UA1E5UAKhCrLjjWfbAMVmldEY7cRKrufVeszOhW3WpS5/1ZrGtq+nK+spsn2mi30PtcswN5uC6eM8lWXzjicWiFlW65LUNjmfHGUtMy8zgjyBMnn9GCY/tdJMh9zM8Gex/gfpLcLxjnn/8a51+Itbr5RdN+M/1/dy5gy1e7VN/77DIiKsdauE/q58AB9QM0pDNrVqyxjeS7Er2nEjtmMlSM8eu4q3QuxD5Sk7H3+n+0TGa4hwmynsHeQewNU9gcb70ea0USN41uRRt0vLYk="
                ]
              }
            ]
          })
        }
    end)

    :ok
  end

  setup do
    {:ok, %{
      valid_token: "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6InNuQTdldFpVVng1d3JaenliS2tsQSJ9.eyJpc3MiOiJodHRwczovL2Rldi00djZodzlmMC51cy5hdXRoMC5jb20vIiwic3ViIjoiYUM5WGZZM0VUYUdSV1I5cWd5WXI2eUJZRmdVWG55eHJAY2xpZW50cyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAxMCIsImlhdCI6MTU5NTI2MzkyMiwiZXhwIjoxNTk1MzUwMzIyLCJhenAiOiJhQzlYZlkzRVRhR1JXUjlxZ3lZcjZ5QllGZ1VYbnl4ciIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.aGd0GClPCB5qDgxAUsfQ905cXRDC5P0rWTCqs5f7tSEbNKhYSir8Tuf7jwP3sF27vkrjZGTPv9ldWmHXgHnt9aAOedi98JWx_9rrTWEjzE88dM3Nj56e6q2te--8rIlXPOz_S930GebqKUzAzJ91O358T7Qu01XMz4oaxVY2CHiaUb_tq5SSD8GOuT1ajhs5WWXrQbyDaFG2OHJ9q4727An_qhCpMoL4aTR6zmWGAocjvV4geizxocMQpULp4VC6UJkpkmVURVfSSp4ZUS7Rkm6SmCq0pz3XpR5BXXO4sgTcFUlYvWi5cvMzAK_bZegrK9OMkoVCZavodrEdnPZMtg",
      token_without_kid: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
      token_with_invalid_kid: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IklOVkFMSUQifQ.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.jEhp7NRedA_JIEQteyiywazRJ_JTXHMOdCSIfRmDGpQ"
    }}
  end

  describe "run/1" do
    test "when token have no kid should return error", %{token_without_kid: token_without_kid} do
      refute true ==
        token_without_kid
        |> Joken.peek_header()
        |> Tuple.to_list()
        |> List.last()
        |> Map.has_key?("kid")

      assert {:error, :token_without_kid} =
        token_without_kid
        |> Jwks.run()
    end

    test "when token is valid should return it", %{valid_token: valid_token} do
      # Joken checks token expiration after check its signature;
      # therefore, tokens returned as expired has a valid signature verified
      assert {:error, [message: "Invalid token", claim: "exp", claim_val: 1_595_350_322]} =
        valid_token
        |> Jwks.run()
    end

    test "when have no keys with kid should return error", %{token_with_invalid_kid: token_with_invalid_kid} do
      assert true ==
        token_with_invalid_kid
        |> Joken.peek_header()
        |> Tuple.to_list()
        |> List.last()
        |> Map.has_key?("kid")

      assert {:error, :kid_does_not_match} =
        token_with_invalid_kid
        |> Jwks.run()
    end
  end

  describe "run/1 when have no cached keys" do
    setup do
      try do
        :ets.delete(Jwks)
      rescue
        _ -> :ok
      end
    end

    test "should fetch from server", %{valid_token: valid_token} do
      assert capture_log(fn -> Jwks.run(valid_token) end) =~
        "fetching keys from server"
    end
  end
end
