defmodule OG.ApiRegistryWeb.ErrorViewTest do
  use ApiRegistryWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(ApiRegistryWeb.ErrorView, "404.json", []) == %{
      error: "not_found",
      message: "Requested resource not found"
    }
  end

  test "renders 500.json" do
    assert render(ApiRegistryWeb.ErrorView, "500.json", []) == %{
      error: "server_error",
      message: "Internal server error",
      reason: "server_error"
    }
  end

  test "renders 500.json with reason" do
    assert render(ApiRegistryWeb.ErrorView, "500.json", [reason: "custom_reason"]) == %{
      error: "server_error",
      message: "Internal server error",
      reason: "custom_reason"
    }
  end
end
