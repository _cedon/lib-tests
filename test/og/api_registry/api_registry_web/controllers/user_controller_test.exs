defmodule OG.ApiRegistryWeb.UserControllerTest do
  use ApiRegistryWeb.ConnCase

  import ApiRegistryWeb.Factory
  import OpenApiSpex.TestAssertions

  setup do
    {:ok,
      params: %{
        name: Faker.Name.PtBr.name(),
        email: Faker.Internet.email(),
        password: "Auda087ea7909AJS"
      }
    }
  end

  describe "POST /v1/users" do
    @tag :rest_post_v1_users
    test "when data is valid should return 201", %{conn: conn, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.user_path(conn, :create)
      conn = post(conn, path, params)
      json = json_response(conn, 201)

      assert_schema(json, "UserResponse", api_spec)
    end

    @tag :rest_post_v1_users
    test "when data is invalid returns 422", %{conn: conn, params: params} do
      invalid = Faker.Address.building_number()
      params  = Map.put(params, :email, invalid)

      path = Routes.user_path(conn, :create)
      conn = post(conn, path, params)

      json_response(conn, 422)
    end
  end

  describe "POST /v1/companies/:company_id/users" do
    setup do
      {:ok, token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJlbWFpbCI6InRoaWFnby5saW1hQHNlbWFudGl4LmNvbS5iciJ9.VALID"}
    end

    setup %{conn: conn, token: token} do
      {:ok, conn: put_req_header(conn, "authorization", "bearer #{token}")}
    end

    setup do
      {:ok, authorization: insert(:authorization)}
    end

    @tag :rest_post_v1_companies_company_id_users
    test "when caller have permission should return 201", %{params: params, conn: conn, authorization: %{profile: profile, company: company}} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()
      params = Map.put(params, "profile_id", profile.id)

      path = Routes.user_path(conn, :create, company.id)
      conn = post(conn, path, params)
      json = json_response(conn, 201)

      assert_schema(json, "UserResponse", api_spec)
    end

    @tag :rest_post_v1_companies_company_id_users
    test "when caller does not have permission should return 403", %{params: params, conn: conn} do
      path = Routes.user_path(conn, :create, Ecto.UUID.generate())
      conn = post(conn, path, params)

      json_response(conn, 403)
    end

    @tag :rest_post_v1_companies_company_id_users
    test "when token is not sent should return 401", %{params: params, conn: conn, authorization: %{company: company}} do
      conn = delete_req_header(conn, "authorization")

      path = Routes.user_path(conn, :create, company.id)
      conn = post(conn, path, params)

      json_response(conn, 401)
    end
  end
end
