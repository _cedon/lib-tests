defmodule OG.ApiRegistryWeb.ProfileControllerTest do
  use ApiRegistryWeb.ConnCase

  import ApiRegistryWeb.Factory
  import OpenApiSpex.TestAssertions

  setup do
    {:ok, token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJlbWFpbCI6InRoaWFnby5saW1hQHNlbWFudGl4LmNvbS5iciJ9.VALID"}
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup %{conn: conn, token: token} do
    {:ok, conn: put_req_header(conn, "authorization", "bearer #{token}")}
  end

  describe "GET /v1/profiles" do
    setup do
      {:ok, profile: insert(:admin_profile)}
    end

    @tag :get_profiles
    test "should return a paginated list", %{conn: conn, profile: %{id: profile_id}} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.profile_path(conn, :index)
      conn = get(conn, path)
      json = json_response(conn, 200)

      assert_schema(json, "ProfilesResponse", api_spec)

      assert %{"entries" => [%{"id" => ^profile_id} | _]} = json
    end
  end

  describe "POST /v1/profiles" do
    setup do
      {:ok,
        params: %{
          id: Ecto.UUID.generate(),
          name: "ADMIN",
          perms: ["read_all", "write_all", "delete_all"],
        }
      }
    end

    @tag :skip
    @tag :create_space
    test "when data is valid, return 201", %{conn: conn, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.profile_path(conn, :create)
      conn = post(conn, path, params)
      json = json_response(conn, 201)

      assert_schema(json, "ProfileResponse", api_spec)
    end

    @tag :skip
    @tag :create_space
    test "when data is invalid, returns 422", %{conn: conn, params: params} do
      params = Map.put(params, :perms, [])

      path = Routes.profile_path(conn, :create)
      conn = post(conn, path, params)

      json_response(conn, 422)
    end
  end
end
