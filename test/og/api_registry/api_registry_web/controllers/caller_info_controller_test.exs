defmodule OG.ApiRegistryWeb.CallerInfoTest do
  @moduledoc false

  use ApiRegistryWeb.ConnCase

  import ApiRegistryWeb.Factory
  import OpenApiSpex.TestAssertions

  setup do
    {:ok, %{user: insert(:user)}}
  end

  setup %{user: %{email: email}} do
    {:ok, %{
      payload: Base.encode64(
        Jason.encode!(%{email: email}),
        padding: false
      )
    }}
  end

  setup %{payload: payload} do
    {:ok, %{token: Enum.join(["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9", payload, "VALID"], ".")}}
  end

  describe "GET /v1/caller_infos when token is valid" do
    setup %{conn: conn, token: token} do
      {:ok, conn: put_req_header(conn, "authorization", "bearer #{token}")}
    end

    @tag :api_get_v1_caller_infos
    test "should return 200 and json", %{conn: conn} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.caller_info_path(conn, :show)
      conn = get(conn, path)
      json = json_response(conn, 200)

      assert_schema(json, "CallerInfoResponse", api_spec)
    end
  end

  describe "GET /v1/caller_infos when token is invalid" do
    setup %{conn: conn} do
      {:ok, conn: put_req_header(conn, "authorization", "bearer INVALIDTOKEN")}
    end

    @tag :api_get_v1_caller_infos
    test "should return 401", %{conn: conn} do
      path = Routes.caller_info_path(conn, :show)
      conn = get(conn, path)

      assert json_response(conn, 401)
    end
  end

  describe "GET /v1/caller_infos when token is not sent" do
    @tag :api_get_v1_caller_infos
    test "should return 401", %{conn: conn} do
      path = Routes.caller_info_path(conn, :show)
      conn = get(conn, path)

      assert json_response(conn, 401)
    end
  end
end
