defmodule OG.ApiRegistryWeb.CompanyControllerTest do
  use ApiRegistryWeb.ConnCase

  import ApiRegistryWeb.Factory
  import OpenApiSpex.TestAssertions

  alias ApiRegistry.Registration.Company

  setup do
    {:ok, token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJlbWFpbCI6InRoaWFnby5saW1hQHNlbWFudGl4LmNvbS5iciJ9.VALID"}
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  setup %{conn: conn, token: token} do
    {:ok, conn: put_req_header(conn, "authorization", "bearer #{token}")}
  end

  describe "POST /v1/companies" do
    setup do
      {:ok,
        user: insert(:user, id: "affe0706-cda3-11ea-87d0-0242ac130003"),
        profile: insert(:admin_profile, name: "ADMIN"),
        params: %{
          document: "69435466000116",
          name: Faker.Team.PtBr.name()
        }
      }
    end

    @tag :create_company
    test "when data is valid, return 201", %{conn: conn, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.company_path(conn, :create)
      conn = post(conn, path, params)
      json = json_response(conn, 201)

      assert_schema(json, "CompanyResponse", api_spec)
    end

    @tag :create_company
    test "when data is invalid should return 422", %{conn: conn, params: params} do
      invalid = Faker.Address.building_number()
      params  = Map.put(params, :document, invalid)

      path = Routes.company_path(conn, :create)
      conn = post(conn, path, params)

      json_response(conn, 422)
    end
  end

  describe "GET /v1/companies/:company_id" do
    setup do
      {:ok, company: insert(:company, id: "c948b2de-7957-432c-b542-d31ddce4df14")}
    end

    @tag :get_company_by_id
    test "when company exists, return 200", %{conn: conn, company: company} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.company_path(conn, :show, company)
      conn = get(conn, path)
      json = json_response(conn, 200)

      assert_schema(json, "CompanyResponse", api_spec)
    end

    @tag :get_company_by_id
    test "when user does not have authorization in company should return 403", %{conn: conn} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()
      company  = %Company{id: Ecto.UUID.generate()}

      path = Routes.company_path(conn, :show, company)
      conn = get(conn, path)
      json = json_response(conn, 403)

      assert_schema(json, "ErrorResponse", api_spec)
    end
  end

  describe "PUT /v1/companies/:company_id" do
    setup do
      {:ok,
        authorization: insert(:authorization),
        params: %{
          document: "14151278000174",
          name: Faker.Team.PtBr.name()
        }
      }
    end

    @tag :update_company
    test "when data is valid should return 200", %{conn: conn, authorization: %{company: %{id: company_id} = company}, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      path = Routes.company_path(conn, :update, company)
      conn = patch(conn, path, params)
      json = json_response(conn, 200)

      assert_schema(json, "CompanyResponse", api_spec)

      assert Map.get(json, "id") == company_id
      assert Map.get(json, "name") == Map.get(params, :name)
      assert Map.get(json, "document") == Map.get(params, :document)
    end

    @tag :update_company
    test "when data is invalid should return 422", %{conn: conn, authorization: %{company: company}, params: params} do
      invalid = Faker.Address.building_number()
      params  = Map.put(params, :document, invalid)

      path = Routes.company_path(conn, :update, company)
      conn = patch(conn, path, params)

      json_response(conn, 422)
    end

    @tag :update_company
    test "when user have no authorization should return 403", %{conn: conn, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      invalid = Faker.Address.building_number()
      params  = Map.put(params, :document, invalid)

      path = Routes.company_path(conn, :update, %Company{id: Ecto.UUID.generate()})
      conn = patch(conn, path, params)
      json = json_response(conn, 403)

      assert_schema(json, "ErrorResponse", api_spec)
    end

    @tag :update_company
    test "when user have only read_all permission should return 403", %{conn: conn, params: params} do
      api_spec = ApiRegistryWeb.ApiSpec.spec()

      invalid = Faker.Address.building_number()
      params  = Map.put(params, :document, invalid)

      path = Routes.company_path(conn, :update, %Company{id: "c4c2030e-88d3-4b63-b697-5116d609cd22"})
      conn = patch(conn, path, params)
      json = json_response(conn, 403)

      assert_schema(json, "ErrorResponse", api_spec)
    end
  end
end
