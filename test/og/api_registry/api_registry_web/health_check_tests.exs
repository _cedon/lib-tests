defmodule OG.ApiRegistryWeb.HealthCheckTest do
  use ApiRegistryWeb.ConnCase

  describe "call/2" do
    test "should return 200", %{conn: conn} do
      assert %Plug.Conn{status: 200} =
        ApiRegistryWeb.HealthCheck.call(conn, [])
    end

    test "should format response as json", %{conn: conn} do
      conn = ApiRegistryWeb.HealthCheck.call(conn, [])
      json = json_response(conn, 200)

      assert json == %{
        "status" => "ok",
        "checks" => []
      }
    end
  end
end
