defmodule OG.ApiRegistry.AuthTest do
  use ApiRegistry.DataCase

  alias ApiRegistry.Auth
  alias ApiRegistry.Auth.Profile

  doctest ApiRegistry.Auth

  describe "create_profile/1" do
    setup do
      {:ok, %{params: %{"name" => "ADMIN", "perms" => ["write_all"]}}}
    end

    @tag :auth_create_profile
    test "when receive empty name returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        params
        |> Map.put("name", "")
        |> Auth.create_profile()
    end

    @tag :auth_create_profile
    test "when receive no name returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        params
        |> Map.drop(["name"])
        |> Auth.create_profile()
    end

    @tag :auth_create_profile
    test "when receive invalid perms returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [perms: {"has an invalid entry", _}]}} =
        params
        |> Map.put("perms", ["write_invalid"])
        |> Auth.create_profile()
    end

    @tag :auth_create_profile
    test "when receive empty perms returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [perms: {"should have at least %{count} item(s)", _}]}} =
        params
        |> Map.put("perms", [])
        |> Auth.create_profile()
    end

    @tag :auth_create_profile
    test "when receive no perms returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [perms: {"can't be blank", _}]}} =
        params
        |> Map.drop(["perms"])
        |> Auth.create_profile()
    end

    @tag :auth_create_profile
    test "when receive duplicate name returns error", %{params: params} do
      assert {:ok, %Profile{}} = Auth.create_profile(params)

      assert {:error, %Ecto.Changeset{errors: [name: {"has already been taken", _}]}} =
        Auth.create_profile(params)
    end

    @tag :auth_create_profile
    test "when receive valid data creates a profile", %{params: params} do
      assert {:ok, %Profile{}} = Auth.create_profile(params)
    end
  end
end
