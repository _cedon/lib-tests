defmodule OG.ApiRegistry.FetcherTest do
  use ApiRegistry.DataCase

  import ApiRegistryWeb.Factory

  alias ApiRegistry.Fetcher

  alias OG.Auth.User

  setup do
    {:ok, %{user: insert(:user)}}
  end

  describe "get_user_by_payload/1" do
    test "when user does not exists should return error" do
      assert {:error, :user_not_found} =
        Fetcher.get_user_by_payload(%{"email" => Faker.Internet.email()})
    end

    test "when user exists should format according behaviour", %{user: %{id: user_id, email: email}} do
      assert {:ok, %User{id: ^user_id, authorizations: []}} =
        Fetcher.get_user_by_payload(%{"email" => email})
    end
  end
end
