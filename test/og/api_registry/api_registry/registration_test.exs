defmodule OG.ApiRegistry.RegistrationTest do
  use ApiRegistry.DataCase

  import ApiRegistryWeb.Factory

  alias ApiRegistry.Repo

  alias ApiRegistry.Registration

  alias ApiRegistry.Registration.Authorization
  alias ApiRegistry.Registration.Company
  alias ApiRegistry.Registration.User

  describe "get_companies_by_user/1" do
    setup do
      {:ok,
        authorization: insert(:authorization),
        user_without_authorizations: insert(:user)
      }
    end

    @tag :registration_get_companies_by_user
    test "should return no companies when user does not have authorization", %{user_without_authorizations: user} do
      assert {:ok, []} =
        Registration.get_companies_by_user(user)
    end

    @tag :registration_get_companies_by_user
    test "should return a list of companies in which user has authorization", %{authorization: %{user: user, company: %{id: company_id}}} do
      assert {:ok, [%Company{id: ^company_id}]} =
        Registration.get_companies_by_user(user)
    end

    @tag :registration_get_companies_by_user
    test "when user does not exists should return exception" do
      assert_raise RuntimeError, ~r/user not found when recovering their companies(.*)/, fn ->
        %User{id: Ecto.UUID.generate()}
        |> Registration.get_companies_by_user()
      end
    end
  end

  describe "get_company/1" do
    setup do
      {:ok, company: insert(:company), profile: insert(:admin_profile)}
    end

    @tag :registration_get_company
    test "when company exists returns the company", %{company: %{id: company_id}} do
      assert {:ok, %Company{id: ^company_id}} = Registration.get_company(company_id)
    end

    @tag :registration_get_company
    test "when company does not exists returns error" do
      assert {:error, :not_found} = Registration.get_company(Ecto.UUID.generate())
    end

    @tag :registration_get_company
    test "when company_id is not a uuid returns error" do
      assert {:error, :not_found} = Registration.get_company(
        :crypto.strong_rand_bytes(30)
        |> Base.encode64()
        |> binary_part(0, 30)
      )
    end
  end

  describe "create_company/2 when another company is already created" do

    setup do
      {:ok, %{user: insert(:user)}}
    end

    setup %{user: user } do
      {:ok, %{
        authorization: insert(:authorization, user: user, profile: build(:admin_profile, name: "ADMIN_2")),
        params: %{
          "name"     => Faker.Company.name()
        }
      }}
    end

    @tag :registration_create_company
    test "when receive valid data and another company already exists, returns error", %{params: params, user: user} do
      assert {:error, :companies_limit_reached} = Registration.create_company(params, user)
    end

  end

  describe "create_company/2" do
    setup do
      {:ok, %{
        user: insert(:user),
        profile: insert(:admin_profile, name: "ADMIN"),
        params: %{
          "name"     => Faker.Company.name(),
          "document" => "07871751000122"
        }
      }}
    end

    @tag :registration_create_company
    test "when receive valid data and does not have any other company created, creates a company", %{params: params, user: user} do
      assert {:ok, %Company{}} = Registration.create_company(params, user)
    end

    @tag :registration_create_company
    test "when receive valid data creates a authorization for creator", %{params: params, user: %{id: user_id} = user} do
      refute ApiRegistry.Repo.get_by(Authorization, user_id: user_id)

      assert {:ok, %Company{id: company_id}} = Registration.create_company(params, user)

      assert ApiRegistry.Repo.get_by(Authorization, user_id: user_id, company_id: company_id)
    end

    @tag :registration_create_company
    test "when receive empty name returns error", %{params: params, user: user} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        params
        |> Map.put("name", "")
        |> Registration.create_company(user)
    end

    @tag :registration_create_company
    test "when receive empty document should register company", %{params: params, user: user} do
      assert {:ok, %Company{document: nil}} =
        params
        |> Map.put("document", "")
        |> Registration.create_company(user)
    end

    @tag :registration_create_company
    test "when receive duplicate document returns error", %{user: user} do
      assert {:error, %Ecto.Changeset{errors: [document: {"has already been taken", _}]}} =
        insert(:company)
        |> Map.from_struct()
        |> Map.take([:name, :document])
        |> Registration.create_company(user)
    end

    @tag :registration_create_company
    test "when receive valid data set status to `ACTIVE`", %{params: params, user: user} do
      refute Map.get(params, "status")

      assert {:ok, %Company{status: "ACTIVE"}} =
        Registration.create_company(params, user)
    end

    test "when receive valid data with status set status to `ACTIVE`", %{params: params, user: user} do
      assert {:ok, %Company{status: "ACTIVE"}} =
        params
        |> Map.put("status", "INACTIVE")
        |> Registration.create_company(user)
    end
  end

  describe "update_company/2" do
    setup do
      {:ok,
        company: insert(:company),
        params: %{
          "name" => Faker.Team.PtBr.name(),
        }
      }
    end

    @tag :registration_update_company
    test "when receive valid data updates the company", %{company: %{id: company_id}, params: params} do
      assert {:ok, %Company{}} = Registration.update_company(company_id, params)
    end

    @tag :registration_update_company
    test "when receive empty name returns error", %{company: %{id: company_id}, params: params} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        Registration.update_company(company_id, Map.put(params, "name", ""))
    end

    @tag :registration_update_company
    test "when receive empty document should update company", %{company: %{id: company_id}, params: params} do
      assert {:ok, %Company{document: nil}} =
        Registration.update_company(company_id, Map.put(params, "document", ""))
    end

    @tag :registration_update_company
    test "when receive duplicate document returns error", %{company: %{id: company_id}, params: params} do
      %Company{document: document} = insert(:company, id: Ecto.UUID.generate())

      assert {:error, %Ecto.Changeset{errors: [document: {"has already been taken", _}]}} =
        Registration.update_company(company_id, Map.put(params, "document", document))
    end

    @tag :registration_update_company
    test "when company_id does not exists returns error", %{params: params} do
      assert {:error, :not_found} = Registration.update_company(Ecto.UUID.generate(), params)
    end
  end

  describe "create_user/1 when receive valid data" do
    setup do
      {:ok,
        params: %{
          "name"     => Faker.Team.PtBr.name(),
          "email"    => Faker.Internet.email(),
          "password" => Faker.Lorem.word()
        }
      }
    end

    setup %{params: params} do
      {:ok, result: Registration.create_user(params)}
    end

    test "should set type to `CUSTOMER`", %{result: result} do
      assert {:ok, %User{type: "CUSTOMER"}} = result
    end

    test "should set status to `ACTIVE`", %{result: result} do
      assert {:ok, %User{status: "ACTIVE"}} = result
    end
  end

  describe "create_user/1 when user has invite" do
    setup do
      {:ok, user: insert(:user, status: "INVITED")}
    end

    setup %{user: user} do
      {:ok, authorization: insert(:authorization, user: user)}
    end

    setup %{user: %{email: email}} do
      {:ok,
        params: %{
          "email" => email,
          "name"  => Faker.Team.name(),

          "password" => Faker.Lorem.word()
        }
      }
    end

    @tag :registration_create_user
    test "should update user status from `INVITED` to `ACTIVE`", %{params: params, user: %{id: user_id} = user} do
      assert user.status == "INVITED"

      assert {:ok, %User{id: ^user_id, status: "ACTIVE"}} =
        Registration.create_user(params)
    end

    @tag :registration_create_user
    test "should preserve authorizations from invite", %{params: params, authorization: %{id: authorization_id}} do
      assert {:ok, %User{}} =
        Registration.create_user(params)

      refute is_nil(Repo.get(Authorization, authorization_id))
    end

    @tag :registration_create_user
    test "should update all user's data", %{params: %{"name" => name, "email" => email} = params, user: %{id: user_id} = user} do
      refute user.name  == name
      assert user.email == email

      assert is_nil(user.password)

      assert {:ok, %User{id: ^user_id, name: ^name, email: ^email}} =
        Registration.create_user(params)
    end
  end

  describe "create_user/1" do
    setup do
      {:ok,
        params: %{
          "name"     => Faker.Team.PtBr.name(),
          "email"    => Faker.Internet.email(),
          "password" => Faker.Lorem.word()
        }
      }
    end

    test "when receive empty name should returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        params
        |> Map.put("name", "")
        |> Registration.create_user()
    end

    test "when receive no name should returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [name: {"can't be blank", _}]}} =
        params
        |> Map.drop(["name"])
        |> Registration.create_user()
    end

    test "when receive empty email should returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [email: {"can't be blank", _}]}} =
        params
        |> Map.put("email", "")
        |> Registration.create_user()
    end

    test "when receive no email should returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [email: {"can't be blank", _}]}} =
        params
        |> Map.drop(["email"])
        |> Registration.create_user()
    end

    test "when receive invalid email should returns error", %{params: params} do
      assert {:error, %Ecto.Changeset{errors: [email: {"has invalid format", _}]}} =
        params
        |> Map.put("email", Faker.Lorem.word())
        |> Registration.create_user()
    end
  end

  describe "invite_user/1 when user with email already exists" do
    setup do
      {:ok, user: insert(:user)}
    end

    setup do
      {:ok, company: insert(:company)}
    end

    setup do
      {:ok, profile: insert(:admin_profile)}
    end

    setup %{company: %{id: company_id}, profile: %{id: profile_id}, user: %{email: email}} do
      {:ok,
        params: %{
          "email" => email,

          "company_id" => company_id,
          "profile_id" => profile_id
        }
      }
    end

    @tag :registration_invite_user
    test "should only creates an authorization", %{user: %{id: user_id}, profile: %{id: profile_id}, params: %{"email" => email, "company_id" => company_id} = params} do
      assert %User{} =
        Repo.get_by(User, email: email)

      assert {:ok, %User{id: ^user_id, email: ^email}} =
        Registration.invite_user(params)

      assert %Authorization{} =
        Authorization
        |> Repo.get_by(company_id: company_id, user_id: user_id, profile_id: profile_id)
    end

    @tag :registration_invite_user
    test "shouldn't update user's information", %{params: params, user: %{name: name}} do
      params =
        params
        |> Map.put("name", Faker.Name.name())

      refute Map.get(params, "name") == name

      assert {:ok, %User{id: user_id, name: ^name}} =
        Registration.invite_user(params)
    end
  end

  describe "invite_user/1 when user with email does not exists" do
    setup do
      {:ok, company: insert(:company)}
    end

    setup do
      {:ok, profile: insert(:admin_profile)}
    end

    setup %{company: %{id: company_id}, profile: %{id: profile_id}} do
      {:ok,
        params: %{
          "email" => Faker.Internet.email(),

          "company_id" => company_id,
          "profile_id" => profile_id
        }
      }
    end

    @tag :registration_invite_user
    test "should create an authorization", %{profile: %{id: profile_id}, params: %{"company_id" => company_id} = params} do
      assert {:ok, %User{id: user_id}} =
        Registration.invite_user(params)

      assert %Authorization{} =
        Authorization
        |> Repo.get_by(company_id: company_id, user_id: user_id, profile_id: profile_id)
    end

    @tag :registration_invite_user
    test "should create user with status `INVITED`", %{params: params} do
      assert {:ok, %User{status: "INVITED"}} =
        Registration.invite_user(params)
    end
  end

  describe "invite_user/1" do
    setup do
      {:ok, %{company: insert(:company)}}
    end

    @tag :registration_invite_user
    test "when profile does not exists should return error", %{company: %{id: company_id}} do
      assert {:error, :profile_not_found} =
        Map.new()
        |> Map.put("company_id", company_id)
        |> Map.put("profile_id", Ecto.UUID.generate())
        |> Registration.invite_user()
    end

    @tag :registration_invite_user
    test "when profile is absent should return error", %{company: %{id: company_id}} do
      assert {:error, :profile_not_found} =
        Map.new()
        |> Map.put("company_id", company_id)
        |> Registration.invite_user()
    end

    @tag :registration_invite_user
    test "when company does not exists should raise exception" do
      assert_raise RuntimeError, ~r/company_id not found when create user(.*)/, fn ->
        Map.new()
        |> Map.put("company_id", Ecto.UUID.generate())
        |> Registration.invite_user()
      end
    end
  end
end
