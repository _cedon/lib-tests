defmodule ApiRegistryWeb.Factory do
  @moduledoc false

  use ExMachina.Ecto, repo: ApiRegistry.Repo

  alias ApiRegistry.Auth.Profile

  alias ApiRegistry.Registration.Authorization
  alias ApiRegistry.Registration.Company
  alias ApiRegistry.Registration.User

  def user_factory do
    %User{
      status: "ACTIVE",
      name: Faker.Name.name(),
      email: Faker.Internet.email(),

      type: "CUSTOMER"
    }
  end

  def company_factory do
    %Company{
      status: "ACTIVE",
      name: Faker.Company.name(),
      document: valid_company_document()
    }
  end

  def admin_profile_factory do
    %Profile{
      name: Faker.Company.name(),
      perms: [
        "read_all",
        "write_all",
        "delete_all"
      ]
    }
  end

  def authorization_factory do
    %Authorization{
      status: "ACTIVE",
      profile: build(:admin_profile, name: "ADMIN"),
      user: build(:user, id: "affe0706-cda3-11ea-87d0-0242ac130003"),
      company: build(:company, id: "c948b2de-7957-432c-b542-d31ddce4df14")
    }
  end

  defp valid_company_document do
    Enum.random([
      "40310122000107",
      "69435466000116",
      "55524296000131",
      "41090117000190",
      "11192648000123"
    ])
  end
end
