defmodule OG.Tests do
  @moduledoc """
  Documentation for `OG.Tests`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> OG.Tests.hello()
      :world

  """
  def hello do
    :world
  end
end
